# Consumer Token Microservice

Generate time-boxed verification tokens for consumers.

## Interfaces

* Generate Token
    * Input: Tenant, ConsumerId, Payload, Time to live
    * Output: JWT Token with surrogate consumer and expiration date
* Verify Token
    * Input: Tenant, JWT Token
    * Output: JWT Token with Tenant, ConsumerId and Payload
* Register Tenant
    * Input: Tenant, Initial Configuration
* Change Configuration
    * Input: Tenant, New Configuration
* Retrieve Configuration
    * Input: Tenant, Effective Period
    * Output: Configuration

## Features

* Multi Tenancy
* Generate JWT Tokens
* Cached Tokens (Optional)
* Persist Tokens (Optional)
* Surrogate Consumer Identifiers (available with Cached and Persisted Tokens)
* Verify JWT Tokens
* Temporal Time-boxed Tenant Secret
* Usage Summary

### Cached Token
Able to cache a token for constant time resolution on token verification.

### Persisted Tokens
Faster token resolution and token generation history available.

## Configurations

* Temporal tenant secret refresh rate
* Required persisted token level
* Default time to live