/**
 * Created by kevinbayes on 10/06/16.
 */

let start = new Date();

import * as redis from 'redis'
import * as jwt from 'jsonwebtoken'

class Consumer {

    constructor(private _id: String) {
    }

    hash() {
        return "fsdf";
    }

    id() {
        return this._id;
    }
}


let client = redis.createClient(6379, "localhost");

client.on("error", function (err) {
    console.log("Error " + err);
});

let partitionTime = new Date();

for(let i = 0; i < 2 * 1000 * 1000; i++) {

    let consumer = new Consumer("sdSDFSDWEFWCE2312323r32r434rFWEFWEFWEEDE");

    client.set(consumer.hash(), JSON.stringify(consumer), (out) => {});

    client.get("string key", (err, result) => {
        //console.log(result);
    });
    
    let token = jwt.sign({usr: consumer.hash()}, "test");

    if(i % 10000 == 0) {
        let timeTaken = (new Date().getTime() - partitionTime.getTime());
        partitionTime = new Date();
        console.log((timeTaken / 10000) * 1000);
    }



    //console.log("");
}

client.quit();

console.log(start.getTime());

console.log((new Date().getTime() - start.getTime()));